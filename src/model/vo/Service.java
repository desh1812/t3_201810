package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	
	//----------------------------------------------------------------------
	// ATRIBUTOS
	//----------------------------------------------------------------------
	
	/**
	 * Atributo que representa el 
	 */
	String dropoffCensusTract;
	
	/**
	 * Atributo que representa
	 */
	String dropoffCentroidLatitude;
	
	/**
	 * Atributo que representa
	 */
	String dropoffCentroidLocation;
	
	
	/**
	 * Atributo que representa
	 */
	String dropoffCentroidLongitude;
	
	/**
	 * Atributo que representa el area com�n donde se baj�
	 */
	int dropoffCommunityArea;
	
	/**
	 * Atributo que representa
	 */
	String extras;
	
	/**
	 * Atributo que representa la tarifa
	 */
	String fare;
	
	/**
	 * Atributo que representa el tipo de pago
	 */
	String paymentType;
	
	/**
	 * Atributo que representa
	 */
	String pickupCensusTract;
	
	/**
	 * Atributo que representa
	 */
	String pickupCentroidLatitude;
	
//	/**
//	 * Atributo que representa
//	 */
//	String pickupCentroidLocation;
	/**
	 * Atributo que representa
	 */
	String pickupCentroidLongitude;
	
	/**
	 * Atributo que representa 
	 */
	String pickUpCommunityArea;
	
	/**
	 * Atributo que representa el id del taxi
	 */
	String taxiId;
	
	/**
	 * Atributo que representa la propina
	 */
	String tips;
	
	/**
	 * Atributo que representa los peajes
	 */
	String tolls;
	
	/**
	 * Atributo que representa 
	 */
	String tripEndTimeStamp;
	
	/**
	 * Atributo que representa el id del viaje
	 */
	String tripId;
	
	/**
	 * Atributo que representa las millas de viaje.
	 */
	double tripMiles;
	/**
	 * Atributo que representa los segundos de viaje
	 */
	int tripSeconds;
	
	/**
	 * Atributo que representa
	 */
	Date tripStartTimestamp;
	
	/**
	 * Atributo que repesenta 
	 */
	double tripTotal;
	
	//----------------------------------------------------------------------------
	// CONSTRUCTOR
	//----------------------------------------------------------------------------
	public Service ( String pDropoffCensusTract, String pDropoffCentroidLatitude, 
			String pDropoffCentroidLocation,
			String pDropoffCentroidLongitude, int pDropoffCommunityArea, String pExtras, String pFare, String pPaymentType, String pPickupCensusTract, String pPickupCentroidLatitude, String pPickupCentroidLongitude, String pPickUpCommunityArea, String pTaxiId, String pTips, String pTolls, String pTripEndTimeStamp, String pTripId, double pTripMiles, int pTripSeconds, Date pTripTimestamp, double pTripTotal)
	{
		dropoffCensusTract = pDropoffCensusTract;
		dropoffCentroidLatitude = pDropoffCentroidLatitude;
		dropoffCentroidLocation = pDropoffCentroidLocation;
		dropoffCentroidLongitude = pDropoffCentroidLongitude;
		dropoffCommunityArea = pDropoffCommunityArea;
		extras = pExtras;
		fare = pFare;
		paymentType = pPaymentType;
		pickupCensusTract = pPickupCensusTract;
		pickupCentroidLatitude = pPickupCentroidLatitude;
//		pickupCentroidLocation = pPickupCentroidLocation;
		pickupCentroidLongitude = pPickupCentroidLongitude;
		pickUpCommunityArea = pPickUpCommunityArea;
		taxiId = pTaxiId;
		tips = pTips;
		tolls = pTolls;
		tripEndTimeStamp = pTripEndTimeStamp;
		tripId = pTripId;
		tripMiles = pTripMiles;
		tripSeconds = pTripSeconds;
		tripStartTimestamp = pTripTimestamp;
		tripTotal = pTripTotal;
	}
	//----------------------------------------------------------------------------
	// M�TODOS
	//----------------------------------------------------------------------------
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	public int getdropoffCommunityArea()
	{
		return dropoffCommunityArea;
	}
	
	public Date getDate()
	{
		return tripStartTimestamp;
	}
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
