package src;

import controller.Controller;
import junit.framework.TestCase;
import model.data_structures.Stack;
import model.data_structures.StackVaciaException;
import model.vo.Taxi;

public class StackTest extends TestCase
{
	//---------------------------------------------------------
	// ATRIBUTOS
	//--------------------------------------------------------
	/**
	 * Controlador para poder cargar los archivos.	
	 */
	Controller controlador = new Controller();

	/**
	 * Lista de taxis tipo lista pila para probar su correcto funcionamiento
	 */
	Stack <Taxi> taxis;
	//--------------------------------------------------------
	// ESCENARIOS
	//--------------------------------------------------------

	/**
	 * Escenario vac�o.
	 */
	private void setupEscenario1()
	{

		taxis = new Stack<Taxi>();
	}
	
	/**
	 * Escenario con la lista de taxis independientes
	 */
	private void setupEscenario2()
	{
		//Reiniciamos la lista por lo que la ponemos en null
		setupEscenario1();
//		controlador.loadServices();
//		ListaDoble<Taxi> lista = controlador.getTaxisOfCompany("Independiente");
//		NodoListaDoble<Taxi> nodo = lista.darPrimero();
//		while(nodo.darSiguiente()!= null)
//		{
//			taxis.a�adir(nodo.darItem());
//			nodo = nodo.darSiguiente();
//		}
	}

	//-----------------------------------------------------------------
	// TESTS
	//-----------------------------------------------------------------

	/**
	 * Test que prueba el correcto funcionamiento de agregar un elemento en una pila
	 */
	public void testAgregarElementos()
	{
		// Pedimos el tama�o a la lista probando que haya 0 elementos al momento de 
		// inicializarla con el escenario 1.
		setupEscenario1();
		assertEquals("El tama�o deber ser 0 porque se acaba de inicializar",0, taxis.darTamanio());
		// A�adimos elementos para probar que est� funcionando como una pila
		// Es decir que est� agregando los elementos al comienzo y el primero que se agregu�
		// est� quedando de �ltimo
		Taxi prueba0 = new Taxi("Prueba1", "Independiente");
		Taxi prueba1 = new Taxi("Prueba2", "Independiente");
		Taxi prueba2 = new Taxi("Prueba3", "Independiente");
		Taxi prueba3 = new Taxi("Prueba2", "Independiente");
		taxis.push(prueba0);
		taxis.push(prueba1);
		taxis.push(prueba2);
		taxis.push(prueba3);
		// Probamos que el primero de la cola sea el primero que se agreg�
		assertEquals("Deber�an ser iguales porque fue el ultimo que se a�adi�",prueba3, taxis.darSuperior().darItem());
		

	}

	/**
	 * Test que prueba el m�todo eliminar Elementos de la pila
	 */
	public void testEliminarElementos()
	{
		//Probamos que nos lance la exception si no tiene elementos con el escenario 0
		setupEscenario1();
		try
		{
			taxis.pop();
			fail("Se esperaba que lanzara Exception");
		}
		catch (Exception e) {

		}
		//Probamos eliminar un elemento, nos debe eliminar los elementos que de ultimo fueron
		//a�adidos
		Taxi prueba0 = new Taxi("Prueba1", "Independiente");
		Taxi prueba1 = new Taxi("Prueba2", "Independiente");
		Taxi prueba2 = new Taxi("Prueba3", "Independiente");
		Taxi prueba3 = new Taxi("Prueba2", "Independiente");
		taxis.push(prueba0);
		taxis.push(prueba1);
		taxis.push(prueba2);
		taxis.push(prueba3);
		try {
			taxis.pop();
		} catch (StackVaciaException e) {

		}
		//Probamos que el elemento superior debe ser igual al penultimo elemento a�adido
		assertEquals("Deben ser los mismos elementos",prueba2, taxis.darSuperior().darItem());

	}
	/**
	 * Test que prueba que se correctamente el tama�o de la pila
	 */
	public void testDarTama�o()
	{
		// Probamos con el escenario 1 con el que nos deben dar 0 elementos
		setupEscenario1();
		assertEquals(0,taxis.darTamanio());
		// Probamos agregando 4 elementos. 
		Taxi prueba0 = new Taxi("Prueba1", "Independiente");
		Taxi prueba1 = new Taxi("Prueba2", "Independiente");
		Taxi prueba2 = new Taxi("Prueba3", "Independiente");
		Taxi prueba3 = new Taxi("Prueba2", "Independiente");
		taxis.push(prueba0);
		taxis.push(prueba1);
		taxis.push(prueba2);
		taxis.push(prueba3);
		assertEquals(4, taxis.darTamanio());
		// Probamos con el escenario 2 en que se debe dar 936 elementos
		// 936 son los taxis que son independientes.
		setupEscenario2();
		assertEquals(936, taxis.darTamanio());
	}
}
