package model.data_structures;

public class Queue  <T extends Comparable>  implements IQueue

{

	//------------------------------------------------------------------
	// ATRIBUTOS
	//------------------------------------------------------------------
	
	/**
	 * Nodo gen�rico que representa el primero de la cola.
	 */
	Nodo <T> primero;
	
	/**
	 * Nodo gen�rico que representa el ultimo de la cola.
	 */
	Nodo <T> ultimo;
	
	/**
	 * Numero que representa el tama�o de la cola.
	 */
	int tamanio;
	
	//------------------------------------------------------------------------
	// CONSTRUCTOR
	//------------------------------------------------------------------------
	/**
	 * Constructor de la lista.
	 * Inicializa primero = null; ultimo = null y tama�o = 0;
	 */
	public Queue()
	{
		primero = null;
		ultimo = null;
		tamanio = 0;
	}
	
	//------------------------------------------------------------------------
	// M�TODOS
	//------------------------------------------------------------------------
	
	/**
	 * M�todo que retorna el primer nodo de la cola
	 * @return Nodo<T> primer nodo de la cola
	 */
	public Nodo<T> darPrimero()
	{
		return primero;
	}
	
	/**
	 * M�todo que retorna el �ltimo nodo de la cola
	 * @return Nodo<T Ultimo nodo de la cola
	 */
	public Nodo<T> darUltimo()
	{
		return ultimo;
	}
	
	public int darTamanio()
	{
		return tamanio;
	}
	/*
	 * M�todo que a�ade elementos a la cola bajo las necesidades que posee una cola, es decir si se
	 * a�ade un elemento se a�ade de �ltimo.
	 */
	public void enqueue(Object item) {
		Nodo <T> nuevoNodo = new Nodo<T> ((Comparable) item);
		if (isEmpty()) {
		primero = nuevoNodo;
		ultimo = nuevoNodo;
		}
		else {
		Nodo <T> viejoUltimo = ultimo;
		viejoUltimo.cambiarSiguiente(nuevoNodo);
		ultimo = nuevoNodo;
		}
		tamanio++;
	}

	/**
	 * M�todo que elimina elementod de la cola bajo las condiciones que una cola presenta, es decir,
	 * elimina el primero que estaba.
	 * @throws QueueVaciaException si la cola est� vac�a 
	 */
	public Object dequeue() throws QueueVaciaException {
		if (isEmpty())
			throw new QueueVaciaException();
		
			Nodo <T> viejoPrimero = primero;
			T elem = primero.darItem();
			primero = viejoPrimero.darSiguiente();
			viejoPrimero.cambiarSiguiente(null);
			tamanio--;
			return elem;
	}

	/**
	 * M�todo que verifica si la lista est� vac�a o no
	 * Retorna True si est� vacia
	 * 			False si no lo est�
	 */
	public boolean isEmpty() {
		if( tamanio == 0 )
		{
			return true;
		}
		else
			return false;
	}

}
