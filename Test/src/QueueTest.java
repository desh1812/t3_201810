package src;

import com.sun.xml.internal.ws.policy.spi.AssertionCreationException;

import controller.Controller;
import junit.framework.TestCase;
import model.data_structures.Nodo;
import model.data_structures.Queue;
import model.data_structures.QueueVaciaException;
import model.vo.Taxi;

public class QueueTest  extends TestCase{

	//---------------------------------------------------------
	// ATRIBUTOS
	//--------------------------------------------------------
	/**
	 * Controlador para poder cargar los archivos.	
	 */
	Controller controlador = new Controller();

	/**
	 * Lista de taxis tipo lista cola para probar su correcto funcionamiento
	 */
	Queue <Taxi> taxis;

	//--------------------------------------------------------
	// ESCENARIOS
	//--------------------------------------------------------

	/**
	 * Escenario vac�o.
	 */
	private void setupEscenario1()
	{

		taxis = new Queue<Taxi>();
	}

	
	/**
	 * Escenario con la lista de taxis independientes
	 */
	private void setupEscenario2()
	{
		//Reiniciamos la lista por lo que la ponemos en null
		setupEscenario1();
//		controlador.loadServices();
//		ListaDoble<Taxi> lista = controlador.getTaxisOfCompany("Independiente");
//		NodoListaDoble<Taxi> nodo = lista.darPrimero();
//		while(nodo.darSiguiente()!= null)
//		{
//			taxis.a�adir(nodo.darItem());
//			nodo = nodo.darSiguiente();
//		}
	}
	
	//-----------------------------------------------------------------
	// TESTS
	//-----------------------------------------------------------------
	
	/**
	 * Test que prueba el correcto funcionamiento de agregar un elemento en una cola
	 */
	public void testAgregarElementos()
	{
		// Pedimos el tama�o a la lista probando que haya 0 elementos al momento de 
		// inicializarla con el escenario 1.
		setupEscenario1();
		assertEquals("El tama�o deber ser 0 porque se acaba de inicializar",0, taxis.darTamanio());
		// A�adimos elementos para probar que est� funcionando como una cola
		// Es decir que est� agregando los elementos al final y el primero que se agregue
		// permanezca siendo el primero
		Taxi prueba0 = new Taxi("Prueba1", "Independiente");
		Taxi prueba1 = new Taxi("Prueba2", "Independiente");
		Taxi prueba2 = new Taxi("Prueba3", "Independiente");
		Taxi prueba3 = new Taxi("Prueba2", "Independiente");
		taxis.enqueue(prueba0);
		taxis.enqueue(prueba1);
		taxis.enqueue(prueba2);
		taxis.enqueue(prueba3);
		// Probamos que el primero de la cola sea el primero que se agreg�
		assertEquals("Deber�an ser iguales porque fue el primero que se agreg�",prueba0, taxis.darPrimero().darItem());
		// Probamos que el �ltimo de la cola sea el �ltimo que se agreg�
		assertEquals("Deber�an ser iguales porque fue el �ltimo que se agreg�",prueba3, taxis.darUltimo().darItem());
		
		
	}
	
	/**
	 * Test que prueba el m�todo eliminar Elementos
	 */
	public void testEliminarElementos()
	{
		//Probamos que nos lance la exception si no tiene elementos con el escenario 0
		setupEscenario1();
		Taxi a�adir1 = new Taxi("EasyTaxy", "Independiente");
		try
		{
			taxis.dequeue();
			fail("Se esperaba que lanzara Exception");
		}
		catch (Exception e) {
			
		}
		//Probamos eliminar un elemento, nos debe eliminar los elementos que primero fueron
		//a�adido
		Taxi prueba0 = new Taxi("Prueba1", "Independiente");
		Taxi prueba1 = new Taxi("Prueba2", "Independiente");
		Taxi prueba2 = new Taxi("Prueba3", "Independiente");
		Taxi prueba3 = new Taxi("Prueba2", "Independiente");
		taxis.enqueue(prueba0);
		taxis.enqueue(prueba1);
		taxis.enqueue(prueba2);
		taxis.enqueue(prueba3);
		try {
			taxis.dequeue();
		} catch (QueueVaciaException e) {
			
		}
		// Ahora el primero debe ser la prueba 2.
		assertEquals("Deber�a ser el mismo elemento",prueba1, taxis.darPrimero().darItem());
		
	}
	/**
	 * Test que prueba que se correctamente el tama�o de la cola
	 */
	public void testDarTama�o()
	{
		// Probamos con el escenario 1 con el que nos deben dar 0 elementos
		setupEscenario1();
		assertEquals(0,taxis.darTamanio());
		// Probamos agregando 4 elementos. 
		Taxi prueba0 = new Taxi("Prueba1", "Independiente");
		Taxi prueba1 = new Taxi("Prueba2", "Independiente");
		Taxi prueba2 = new Taxi("Prueba3", "Independiente");
		Taxi prueba3 = new Taxi("Prueba2", "Independiente");
		taxis.enqueue(prueba0);
		taxis.enqueue(prueba1);
		taxis.enqueue(prueba2);
		taxis.enqueue(prueba3);
		assertEquals(4, taxis.darTamanio());
		// Probamos con el escenario 2 en que se debe dar 936 elementos
		// 936 son los taxis que son independientes.
		setupEscenario2();
		//assertEquals(936, taxis.darTamanio());
	}
	
	
}
