package model.data_structures;

public class Nodo <T extends Comparable> 
{
	
	//------------------------------------------------------------
	// ATRIBUTOS
	//------------------------------------------------------------
	/**
	 * Nodo siguiente
	 */
	private Nodo<T> siguiente;
	
	/**
	 * Item con la informaci�n del nodo
	 */
	private T item;
	
	//-------------------------------------------------------------
	// CONSTRUCTURES
	//-------------------------------------------------------------
	/**
	 * Constructor 
	 * @param item2 Objeto el cual vamos a usar
	 */
	public Nodo (Comparable item2) 
	{
		this.item = (T) item2;
		this.siguiente = null;
	}
	/**
	 * M�todo por el cual devuelvo el siguiente de la lista
	 * @return Nodo siguiente
	 */
	public Nodo<T> darSiguiente() 
	{
	return siguiente;
	}
	
	/**
	 * M�todo que inserta en la posici�n siguiente.
	 * @param next
	 */
	public void cambiarSiguiente ( Nodo<T> siguiente ) 
	{
		this.siguiente = siguiente ;
	}
	
	/**
	 * Retorna el item que contiene el ndo
	 * @return item que contiene el nodo
	 */
	public T darItem()
	{
		return item;
	}
	/**
	 * Inserta el item en el nodo
	 * @param item. Item a insertar
	 */
	public void cambiarItem (T item) 
	{
		this.item = item;
	}
}
