package model.data_structures;

public class Stack <T extends Comparable<T>> implements IStack{

	//-----------------------------------------------------------------
	// ATRIBUTOS
	//-----------------------------------------------------------------
	/**
	 * Hace referencia al nodo que se encuentra en la parte superior de la pila
	 */
	private Nodo<T> superior;

	/**
	 * Atributo que hace referencia al tama�ano de la pila (Cantidad de elementos)
	 */
	private int tamanio;

	//-----------------------------------------------------------------
	// CONSTRUCTOR
	//-----------------------------------------------------------------
	/**
	 * M�todo que crear una lista pila. 
	 */
	public Stack()
	{
		superior = null;
		tamanio = 0;
	}

	//-----------------------------------------------------------------
	// M�TODOS
	//-----------------------------------------------------------------
	/**
	 * M�todo que retorna el Nodo superior de la pila.
	 * @return Nodo<T> Elemento superior de la pila.
	 */
	public Nodo<T> darSuperior()
	{
		return superior;
	}
	
	/**
	 * M�todo que retorna el tama�o de la pila.
	 * @return Nodo<T> Elemento superior de la pila.
	 */
	public int darTamanio()
	{
		return tamanio;
	}
	
	
	/**
	 * M�todo que se encarga de a�adir elementos a la pila. Es decir en la primera posici�n.
	 */
	public void push(Object item) {
		Nodo <T> newNode = new Nodo<T> ((Comparable) item);
		if (superior == null )
		superior = newNode;
		else {
		newNode.cambiarSiguiente(superior);
		superior = newNode;
		}
		tamanio++;
	}

	/**
	 * M�todo que se encarga de eliminar el primer elemento de la pila. 
	 * @throws StackVaciaException Si la lista est� vac�a.
	 */
	public Object pop() throws StackVaciaException {
		
		if (superior == null )
			throw new StackVaciaException();
			
			
			T elem = superior.darItem();
			Nodo <T> nexTop = superior.darSiguiente();
			superior.cambiarSiguiente(null);
			superior = nexTop;
			tamanio--;
			return elem;
	}

	@Override
	public boolean isEmpty() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return false;
	}

}
